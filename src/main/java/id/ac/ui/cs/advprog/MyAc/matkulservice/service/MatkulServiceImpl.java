package id.ac.ui.cs.advprog.MyAc.matkulservice.service;

import id.ac.ui.cs.advprog.MyAc.matkulservice.model.Matkul;
import id.ac.ui.cs.advprog.MyAc.matkulservice.repository.MatkulRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MatkulServiceImpl implements MatkulService{

    @Autowired
    private MatkulRepository matkulRepository;

    @Override
    public List<Matkul> findAll() {
        return matkulRepository.findAll();
    }

    @Override
    public Optional<Matkul> findMatkul(String kode) {
        return matkulRepository.findById(kode);
    }

    @Override
    public List<Matkul> findMatkulBySemester(int semester) {
        return matkulRepository.findMatkulBySemester(semester);
    }

    @Override
    public List<Matkul> findMatkulByNama(String nama) {
        return matkulRepository.findMatkulByNama(nama.toLowerCase());
    }

    @Override
    public List<Matkul> findMatkulBySks(int sks) {
        return matkulRepository.findMatkulBySks(sks);
    }

    @Override
    public List<Matkul> findMatkulBySemesterAndNama(int semester, String nama) {
        return matkulRepository.findMatkulBySemesterAndNama(semester, nama.toLowerCase());
    }

    @Override
    public List<Matkul> findMatkulBySemesterAndSks(int semester, int sks) {
        return matkulRepository.findMatkulBySemesterAndSks(semester, sks);
    }

    @Override
    public List<Matkul> findMatkulBySksAndNama(int sks, String nama) {
        return matkulRepository.findMatkulBySksAndNama(sks, nama.toLowerCase());
    }

    @Override
    public List<Matkul> findMatkulBySemesterAndNamaAndSks(int semester, String nama, int sks) {
        return matkulRepository.findMatkulBySemesterAndNamaAndSks(semester, nama.toLowerCase(), sks);
    }

    @Override
    public void erase(String kode) {
        matkulRepository.deleteById(kode);
    }

    @Override
    public Matkul rewrite(Matkul matkul) {
        matkulRepository.deleteById(matkul.getKode());
        return matkulRepository.save(matkul);
    }

    @Override
    public Matkul register(Matkul matkul) {
        return matkulRepository.save(matkul);
    }
}
