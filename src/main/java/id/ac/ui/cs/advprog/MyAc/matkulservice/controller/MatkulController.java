package id.ac.ui.cs.advprog.MyAc.matkulservice.controller;

import id.ac.ui.cs.advprog.MyAc.matkulservice.model.Matkul;
import id.ac.ui.cs.advprog.MyAc.matkulservice.service.MatkulService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(path = "/matkul")
public class MatkulController {

    @Autowired
    private MatkulService matkulService;

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping
    public ResponseEntity<List<Matkul>> findAll(){
        return new ResponseEntity<>(matkulService.findAll(), HttpStatus.OK);
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping
    public ResponseEntity create(@RequestBody Matkul matkul){
        matkulService.register(matkul);
        return new ResponseEntity(HttpStatus.OK);
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/{kode}")
    public ResponseEntity<Matkul> findById(@PathVariable String kode){
        Optional<Matkul> optionalMatkul = matkulService.findMatkul(kode);

        if(!optionalMatkul.isPresent()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Matkul>(optionalMatkul.get(),HttpStatus.OK);

    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/semester={semester}")
    public ResponseEntity<List<Matkul>> findMatkulBySemester(@PathVariable int semester){
        List<Matkul> listMatkul = matkulService.findMatkulBySemester(semester);

        if(listMatkul.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity(listMatkul, HttpStatus.OK);

    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/sks={sks}")
    public ResponseEntity<List<Matkul>> findMatkulBySks(@PathVariable int sks){
        List<Matkul> listMatkul = matkulService.findMatkulBySks(sks);

        if(listMatkul.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity(listMatkul, HttpStatus.OK);

    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/nama={nama}")
    public ResponseEntity<List<Matkul>> findMatkulByNama(@PathVariable String nama){
        List<Matkul> listMatkul = matkulService.findMatkulByNama(nama);

        if(listMatkul.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity(listMatkul, HttpStatus.OK);

    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/semester={semester}/nama={nama}")
    public ResponseEntity<List<Matkul>> findMatkulBySemesterAndNama(@PathVariable int semester, @PathVariable String nama){
        List<Matkul> listMatkul = matkulService.findMatkulBySemesterAndNama(semester, nama);

        if(listMatkul.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity(listMatkul, HttpStatus.OK);

    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/semester={semester}/sks={sks}")
    public ResponseEntity<List<Matkul>> findMatkulBySemesterAndSks(@PathVariable int semester, @PathVariable int sks){
        List<Matkul> listMatkul = matkulService.findMatkulBySemesterAndSks(semester, sks);

        if(listMatkul.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity(listMatkul, HttpStatus.OK);

    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/sks={sks}/nama={nama}")
    public ResponseEntity<List<Matkul>> findMatkulBySksAndNama(@PathVariable int sks, @PathVariable String nama){
        List<Matkul> listMatkul = matkulService.findMatkulBySksAndNama(sks, nama);

        if(listMatkul.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity(listMatkul, HttpStatus.OK);

    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/semester={semester}/nama={nama}/sks={sks}")
    public ResponseEntity<List<Matkul>> findMatkulBySemesterAndNamaAndSks(@PathVariable int semester, @PathVariable String nama, @PathVariable int sks){
        List<Matkul> listMatkul = matkulService.findMatkulBySemesterAndNamaAndSks(semester, nama, sks);

        if(listMatkul.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity(listMatkul, HttpStatus.OK);

    }


}