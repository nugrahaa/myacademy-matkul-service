package id.ac.ui.cs.advprog.MyAc.matkulservice.repository;

import id.ac.ui.cs.advprog.MyAc.matkulservice.model.Matkul;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MatkulRepository extends JpaRepository<Matkul, String> {

    @Query("SELECT m FROM Matkul m where lower(m.nama) like %:nama%")
    List<Matkul> findMatkulByNama(@Param("nama") String nama);

    @Query("SELECT m FROM Matkul m WHERE m.semester = :semester")
    List<Matkul> findMatkulBySemester(@Param("semester") int semester);

    @Query("SELECT m FROM Matkul m WHERE m.sks = :sks")
    List<Matkul> findMatkulBySks(@Param("sks") int sks);

    @Query("SELECT m FROM Matkul m WHERE m.semester = :semester AND lower(m.nama) like %:nama%")
    List<Matkul> findMatkulBySemesterAndNama(@Param("semester") int semester, @Param("nama") String nama);

    @Query("SELECT m FROM Matkul m WHERE m.semester = :semester AND m.sks = :sks")
    List<Matkul> findMatkulBySemesterAndSks(@Param("semester") int semester, @Param("sks") int sks);

    @Query("SELECT m FROM Matkul m WHERE m.sks = :sks AND lower(m.nama) like %:nama%")
    List<Matkul> findMatkulBySksAndNama(@Param("sks") int semester, @Param("nama") String nama);

    @Query("SELECT m FROM Matkul m WHERE m.semester = :semester AND lower(m.nama) like %:nama% AND m.sks = :sks")
    List<Matkul> findMatkulBySemesterAndNamaAndSks(@Param("semester") int semester, @Param("nama") String nama, @Param("sks") int sks);

}
