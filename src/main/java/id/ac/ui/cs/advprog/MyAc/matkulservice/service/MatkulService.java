package id.ac.ui.cs.advprog.MyAc.matkulservice.service;

import id.ac.ui.cs.advprog.MyAc.matkulservice.model.Matkul;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

public interface MatkulService {

    List<Matkul> findAll();

    Optional<Matkul> findMatkul(String kode);

    List<Matkul> findMatkulBySemester(int semester);
    List<Matkul> findMatkulByNama(String nama);
    List<Matkul> findMatkulBySks(int sks);
    List<Matkul> findMatkulBySemesterAndNama(int semester, String nama);
    List<Matkul> findMatkulBySemesterAndSks(int semester, int sks);
    List<Matkul> findMatkulBySksAndNama(int sks, String nama);
    List<Matkul> findMatkulBySemesterAndNamaAndSks(int semester, String nama, int sks);


    void erase(String kode); //delete

    Matkul rewrite(Matkul matkul); //update

    Matkul register(Matkul matkul); //create

}
